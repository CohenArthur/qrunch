#ifndef STRUCT_QR_H
#define STRUCT_QR_H

#include "xalloc.h"
#include "matrix.h"

typedef struct qrcode
{
    int type;
    doubleMatrix *matrix;
    int mask;
	char* encoding_mode;
    int message_len;
    char * data;
}Qrcode;

void initqr(Qrcode *qr, doubleMatrix *matrix);

#endif
