#ifndef POLY_H
#define POLY_H

#include "xalloc.h"
#include "list.h"

struct poly
{
    struct link_list *coef; //list coef
    int degree;
};

struct poly *new_poly(int degree);

void gf_print_poly(struct poly *p);

int gf_getval_poly(struct poly *p, int pos);

void gf_changeval_poly(struct poly *p, int pos, int val);

struct poly *gf_poly_mult(struct poly *p, struct poly *q);

void free_poly(struct poly *poly);

#endif
