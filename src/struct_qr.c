#include <stdio.h>
#include <stdlib.h>

#include "struct_qr.h"
#include "matrix.h"
#include "decode.h"

void initqr(Qrcode *qr, doubleMatrix *matrix)
{
    //qr = xmalloc(sizeof (Qrcode));
    //qr->matrix = xmalloc(sizeof(doubleMatrix));
    qr->type			 = ((int)(matrix->rows) - 17)/4; 
    qr->matrix			 = matrix;
   
    qr->mask = get_mask(qr);
    char*  encoding_pointeur = NULL;
    get_encoding_mode(qr,&encoding_pointeur);

    qr->encoding_mode = encoding_pointeur;
    
    qr->message_len		 = get_message_len(qr);
    qr->data			 = get_data(qr);

	//Use qr->matrix->rows to get the size of the matrix/QR Code.
	//Use qr->matrix->data to access the matrix's data.
}


void freeqr(Qrcode *qr)
{

	doublematrix_free(qr->matrix); 
	free(qr->data);
	free(qr); 
}

