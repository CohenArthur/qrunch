#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include "GFb.h"
#include "list.h"

/*For each possible value in the galois field 2^8,
we will pre-compute the logarithm and 
anti-logarithm (exponential) of this value*/

u_int16_t gf_exp[256];    //exponential table
u_int16_t gf_log[512];    //logarithm table

/*uint8_t gf_mult_noLUT(x, y, prim, int carryless)

Galois Field integer multiplication using 
Russian Peasant Multiplication algorithm 
(faster than the standard multiplication + modular reduction).

{
    uint8_t r = 0;

    while(y > 0)
    {
        if(y & 1)
        {
            if(carryless)
            {
                r = r ^ x;    
            }
            else
            {
                r = r + x;    
            }

            y = y >> 1;
            x = x << 1;

            if(prim > 0 && x & 256)
            {
                x = x ^ prim;    
            }
        }        
    }

    return r;
}*/



void init_tables()

/*Precompute the logarithm and anti-log tables for faster computation later, 
using the provided primitive polynomial.*/

{
    //gf_exp = xmalloc(sizeof(u_int8_t)*256);
    //gf_log = xmalloc(sizeof(u_int8_t)*512);

    for(u_int16_t i = 0; i < 255; i++)
    {
        gf_exp[i] = 0;
        gf_log[i] = 0;
    }

    u_int16_t x = 1;

    for(u_int16_t i = 0; i < 255; i++)
    {
        gf_exp[i] = x;
        gf_log[x] = i;
        
        x <<= 1;
        
        if(x & 0x100)
        {            
            x ^= 0x11d;
        }
    }

    for(u_int16_t i = 0; i < 255; i++)
    {
        gf_exp[i+255] = gf_exp[i];
    }
}

void print_tables(int b_exp, int b_log)
{
    if(b_exp == 1)
    {
        int exp = 0;

        while(exp < 256)
        {
            printf("gf_exp[%d] = %d \n", exp, gf_exp[exp]);
            exp++;
        }

        printf("\n");

    }
    else
    {
        printf("NO GF_EXP PRINTING \n");
        printf("\n");
    }

    if(b_log == 1)
    {
        int log = 0;

        while(log < 256)
        {
            printf("gf_log[%d] = %d \n", log, gf_log[log]);
            log++;
        }

        printf("\n");
    }
    else
    {
        printf("NO GF_LOG PRINTING \n");
        printf("\n");
    }
}

u_int8_t gf_add(u_int8_t x, u_int8_t y)
{
    return x ^ y;
}

u_int8_t gf_sub(u_int8_t x, u_int8_t y)
{
    return x ^ y;
}

/*uint8_t bit_length(uint8_t b)
{
    uint8_t bits = 0;

    while(b >> bits)
    {
        bits += 1;    
    }

    return bits;
    
}*/

u_int8_t gf_mult(u_int8_t x, u_int8_t y)
{
    if(x == 0 || y == 0)
    {
        return 0;    
    }

    return gf_exp[gf_log[x] + gf_log[y]];
}


/*uint8_t gf_div(uint8_t x, uint8_t y)
{
    if(y == 0)
    {
        err(42,"DIVISION BY ZERO");    
    }

    if(x == 0)
    {
        return 0;       
    }

    return gf_exp[(gf_log[x] + 255 - gf_log[y]) %n 255];

}*/

u_int8_t gf_pow(u_int8_t x, u_int8_t power)
{
    return gf_exp[(gf_log[x] * power) % 255];    
}

/*uint8_t gf_inverse(x)
{
    return gf_exp[255 - gf_log[x]]; //gf_inverse(x) == gf_div(1, x)   
}*/

