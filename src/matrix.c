#include "matrix.h"

//___________________________________________________________________________//
//______________________________MATRIX_______________________________________//
//___________________________________________________________________________//

matrix *matrix_init(unsigned rows, unsigned columns){
	matrix *new_matrix = xmalloc(sizeof(matrix));
	int *array = xcalloc(1, rows * columns * sizeof(int));

	if (array == NULL || new_matrix == NULL){
		err(1, "Allocation of the matrix failed.");
	}

	new_matrix->rows	 = rows;
	new_matrix->columns	 = columns;
	new_matrix->data	 = array;

	return new_matrix;
}

void matrix_free(matrix *matrix){
	free(matrix->data);
	free(matrix);
}

matrix *matrix_resize(matrix *old_matrix){
	size_t index_diag = 0;
	size_t factor;

	while(old_matrix->data[index_diag + index_diag * old_matrix->columns] != 0){
		index_diag++;
	}

	factor = index_diag;

	if (factor == 0)
			errx(1,
"Computing the resizing factor failed (matrix_resize). Crop isn't good enough.");

	if (old_matrix->rows % factor != 0)
			factor++;

	unsigned new_rows	 = old_matrix->rows 	/ factor;
	unsigned new_columns = old_matrix->columns 	/ factor;

	unsigned new_index = 0;

	matrix *new_matrix;
	new_matrix = matrix_init(new_rows, new_columns);

	FOR_EACH(old_matrix, factor);
		if (old_matrix->data[index_w + index_h * old_matrix->columns] != 0){
			new_matrix->data[new_index] = 1;
		}
		new_index++;
	END_FE;

	matrix_free(old_matrix);
	return new_matrix;
}

void print_matrixInfo(matrix *matrix){
	printf("Matrix has %d rows.\nMatrix has %d columns.\n",
					matrix->rows, matrix->columns);
}

void print_matrix(matrix *matrix){
	FOR_EACH(matrix, 1);
			if (matrix->data[index_w + index_h * matrix->columns] != 1)
				printf(" ,");
			else
				printf("1,");
		}
		printf("\n");
	}
}

void print_matrix_array(matrix *matrix){
	printf("[");
	for (unsigned index = 0; index < matrix->rows * matrix->columns; index++){
		printf("%d,", matrix->data[index]);
	}
	printf("]\n");
}

//___________________________________________________________________________//
//_____________________________DOUBLE_MATRIX_________________________________//
//___________________________________________________________________________//


doubleMatrix *doublematrix_init(unsigned rows, unsigned columns){
	doubleMatrix *new_matrix = xmalloc(sizeof(doubleMatrix));
	int **array = xcalloc(1, sizeof(*array) * rows);

	for (unsigned index = 0; index < rows; index++){
		array[index] = xcalloc(1,sizeof(int) * columns);
	}
	new_matrix->rows	 = rows;
	new_matrix->columns	 = columns;
	new_matrix->data	 = array;

	return new_matrix;
}

void doublematrix_free(doubleMatrix *doubleMatrix){
	for (unsigned index = 0; index < doubleMatrix->rows; index++){
		free(doubleMatrix->data[index]);
	}

	free(doubleMatrix->data);
	free(doubleMatrix);
}

doubleMatrix *simple_to_double(matrix *old_matrix){
	doubleMatrix *new_matrix;
	new_matrix = doublematrix_init(old_matrix->rows, old_matrix->columns);

	FOR_EACH(old_matrix, 1);
		if (old_matrix->data[index_w + index_h * old_matrix->columns] != 0)
			new_matrix->data[index_w][index_h] = 1;
	END_FE;

	return new_matrix;
}

doubleMatrix *doublematrix_scale_up(doubleMatrix *matrix, size_t factor){
	doubleMatrix *result_matrix = doublematrix_init(matrix->rows * factor, matrix->columns * factor);

	FOR_EACH(matrix, 1)
		for(size_t result_w = index_w * factor; result_w < (index_w * factor) + factor; result_w++){
			for(size_t result_h = index_h * factor ; result_h < (index_h * factor) + factor; result_h++){
				result_matrix->data[result_h][result_w] = matrix->data[index_w][index_h];
			}
		}
	END_FE;
	return result_matrix;
}

void print_double_matrix(doubleMatrix *doubleMatrix){
	FOR_EACH(doubleMatrix, 1);
			if (doubleMatrix->data[index_w][index_h] != 1)
				printf(" ,");
			else
				printf("1,");
		}
	printf("\n");
	}
}

static void draw_cube(doubleMatrix *dest_matrix, size_t x, size_t y){

	size_t index_w = x;
	size_t index_h = y;

	for(index_w = x; index_w < x + 3; index_w++)
		for(index_h = y; index_h < y + 3; index_h++)
			SET_TO_ONE;
}

static void draw_block(doubleMatrix *dest_matrix, size_t x, size_t y){
	size_t index_w = x;
	size_t index_h = y;

	for(size_t _ = 0; _ < 2; _++){
		for(index_w = x; index_w < x + 7; index_w++){
			SET_TO_ONE;
		}

		index_h = y + 6;
	}

	index_w = x;
	index_h = y;

	for(size_t _ = 0; _ < 2; _++){
		for(index_h = y; index_h < y + 7; index_h++){
			SET_TO_ONE;
		}

		index_w = x + 6;
	}

	draw_cube(dest_matrix, x + 2, y + 2);
}

static void draw_alter_line(doubleMatrix *dest_matrix, size_t pos, int dir){
	size_t max = 0;

	if (dir == HORIZONTAL) {
		max = dest_matrix->columns;
	} else if (dir == VERTICAL) {
		max = dest_matrix->rows;
	} else
		errx(1, "Wrong parameter for function draw_alter_line()");

	for(size_t index = 0; index < max; index++)
		if (index % 2 == 0) {
			if (dir == VERTICAL)
				dest_matrix->data[index][pos] = 1;
			else
				dest_matrix->data[pos][index] = 1;
		}
}

doubleMatrix *doubleMatrix_init_blocks(unsigned rows, unsigned columns){
	doubleMatrix *result = doublematrix_init(rows, columns);

	draw_block(result, 0, 0);
	draw_block(result, result->rows - 7, 0);
	draw_block(result, 0, result->columns - 7);

	draw_alter_line(result, 6, VERTICAL);
	draw_alter_line(result, 6, HORIZONTAL);

	return result;
}
