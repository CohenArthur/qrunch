#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "xalloc.h"

/*
 * Sauvegarde une surface au format BMP, au nom filename
 * Le filename peut etre un .bmp ou un .png
 */
void surface_to_image(SDL_Surface *input_surface, char *filename);
