#ifndef SDL_FUNCTIONS_H
#define SDL_FUNCTIONS_H

#include "xalloc.h"
#include <err.h>

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_rotozoom.h"

/*
 * Initialise la bibliotheque SDL
 */
void init_sdl();

/*
 * Charge l'image situee a l'emplacement 'path' dans une SDL_Surface
 */
SDL_Surface *load_image(char *path);

/*
 * Affiche l'image 'img' sur la Surface retournee
 */
SDL_Surface *display_image(SDL_Surface *img);

/*
 * Attend qu'une touche soit pressee puis relachee
 */
void wait_for_keypressed();

/*
 * Rogne une surface du point (x, y) jusqu'au point (x + width, y + height)
 */
SDL_Surface *crop_surface(SDL_Surface *input_image, int x, int y, int width, int height);

#endif
