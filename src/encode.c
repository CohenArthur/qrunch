#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "decode.h"
#include "encode.h"

#include "Reed_S.h"

void bii(unsigned int n, int * binvalue)
{
    int len = 0;
    for(int shift=7 /*sizeof(int)*8-1*/;shift>=0;shift--)
    {
        if (n >> shift & 1)
        binvalue[len] = 1;
        else
        binvalue[len] = 0;
        len++;

    }
    //printf("\n");
}
/*
void insert_len(Qrcode qr)
{
    int * biit = xcalloc(8,sizeof(int));
    bii(qr.message_len,biit);
    set_carreV(qr,biit,qr->matrix->rows-3,qr->matrix->columns-1,1);
    int index = 0;
    for (int x = qr.matrix->rows-6; x < (int)qr.matrix->rows-2; x++)
    {
        for (int y = qr.matrix->columns-2; y < (int)qr.matrix->columns; y++)
        {
            int m = qr.mask;
            int invert = 0;
            switch (m) {
                case 0:  invert = (y + x) % 2 == 0;                    break;
                case 1:  invert = x % 2 == 0;                          break;
                case 2:  invert = y % 3 == 0;                          break;
                case 3:  invert = (y + x) % 3 == 0;                    break;
                case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
            }
            if(invert)
                qr.matrix->data[x][y] = !biit[index]; //!
            else
                qr.matrix->data[x][y] = biit[index];
            index++;
        }
    }

}
*/
int set_bitmask(Qrcode *qr,int x,int y,int bit)
{
    int m = qr->mask;
    int invert = 0;
    switch (m) {
        case 0:  invert = (y + x) % 2 == 0;                    break;
        case 1:  invert = x % 2 == 0;                          break;
        case 2:  invert = y % 3 == 0;                          break;
        case 3:  invert = (y + x) % 3 == 0;                    break;
        case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
        case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
        case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
        case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
    }
    if (invert)
    {
        return !bit;
    }
    return bit;
}

void insert_mask(Qrcode * qr)
{
	int biit[8] = {0,0,0,0,0,0,0,0};
	bii(qr->mask,biit);
	int mask[3] = {!biit[5],biit[6],!biit[7]};
	qr->matrix->data[8][2] = mask[0];
	qr->matrix->data[8][3] = mask[1];
	qr->matrix->data[8][4] = mask[2];

	int r = qr->matrix->rows;
	int c = qr->matrix->columns;
	qr->matrix->data[r-3][c-13] = mask[0];
	qr->matrix->data[r-4][c-13] = mask[1];
	qr->matrix->data[r-5][c-13] = mask[2];

	// insert correction lvl juste L;
	qr->matrix->data[8][0] = 1;
	qr->matrix->data[8][1] = 1;
	qr->matrix->data[r-1][c-13] = 1;
	qr->matrix->data[r-2][c-13] = 1;

	qr->matrix->data[r-6][c-13] = 1; //5
	qr->matrix->data[r-7][c-13] = 1; //6
	qr->matrix->data[r-8][c-13] = 1; //juste point
	qr->matrix->data[8][13]     = 1; //7
	qr->matrix->data[8][14]     = 1; //8
	qr->matrix->data[8][15]     = 0; //9
	qr->matrix->data[8][16]     = 0; //10
	qr->matrix->data[8][17]     = 0; //11
	qr->matrix->data[8][18]     = 1; //12
	qr->matrix->data[8][19]     = 0; //13
	qr->matrix->data[8][20]     = 0; //14


	qr->matrix->data[8][5] = 1; //5
	qr->matrix->data[8][7] = 1; //6
	qr->matrix->data[8][8] = 1; //7
	qr->matrix->data[7][8] = 1; //8
	qr->matrix->data[5][8] = 0; //9
	qr->matrix->data[4][8] = 0; //10
	qr->matrix->data[3][8] = 0; //11
	qr->matrix->data[2][8] = 1; //12
	qr->matrix->data[1][8] = 0; //13
	qr->matrix->data[0][8] = 0; //14



	for(int i = 7; i<9; i++)
	{
	    for (int j = 9; j<10;j++)
	    {
		qr->matrix->data[i][j] = set_bitmask(qr,i,j,0);
	    }
	}




}

void set_carreH(Qrcode *qr,int * bit, int i, int j,int direction)
{
    if(direction)
    {
        qr->matrix->data[i][j]     = set_bitmask(qr,i,j,bit[0]);
        qr->matrix->data[i][j-1]   = set_bitmask(qr,i,j-1,bit[1]);
        qr->matrix->data[i-1][j]   = set_bitmask(qr,i-1,j,bit[2]);
        qr->matrix->data[i-1][j-1] = set_bitmask(qr,i-1,j-1,bit[3]);
        qr->matrix->data[i-1][j-2] = set_bitmask(qr,i-1,j-2,bit[4]);
        qr->matrix->data[i-1][j-3] = set_bitmask(qr,i-1,j-3,bit[5]);
        qr->matrix->data[i][j-2]   = set_bitmask(qr,i,j-2,bit[6]);
        qr->matrix->data[i][j-3]   = set_bitmask(qr,i,j-3,bit[7]);

    }
    else
    {
        qr->matrix->data[i][j]     = set_bitmask(qr,i,j,bit[2]);
        qr->matrix->data[i][j-1]   = set_bitmask(qr,i,j-1,bit[3]);
        qr->matrix->data[i-1][j]   = set_bitmask(qr,i-1,j,bit[0]);
        qr->matrix->data[i-1][j-1] = set_bitmask(qr,i-1,j-1,bit[1]);
        qr->matrix->data[i-1][j-2] = set_bitmask(qr,i-1,j-2,bit[6]);
        qr->matrix->data[i-1][j-3] = set_bitmask(qr,i-1,j-3,bit[7]);
        qr->matrix->data[i][j-2]   = set_bitmask(qr,i,j-2,bit[4]);
        qr->matrix->data[i][j-3]   = set_bitmask(qr,i,j-3,bit[5]);

    }
}

void set_carreV(Qrcode *qr,int * bii,int i, int j,int direction)
{

    int rmax = 0;
    int cmax = 0;
    if(direction)
    {
        rmax = i-4;
        cmax = j-2;
	int index = 0;
        for (int x = i; x > rmax ; x--)
        {
            for (int y = j; y > cmax ; y--)
            {
                int m = qr->mask;
                int invert = 0;
                switch (m) {
                    case 0:  invert = (y + x) % 2 == 0;                    break;
                    case 1:  invert = x % 2 == 0;                          break;
                    case 2:  invert = y % 3 == 0;                          break;
                    case 3:  invert = (y + x) % 3 == 0;                    break;
                    case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                    case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                    case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                    case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
                }
                if(invert)
		{
                    qr->matrix->data[x][y] = !bii[index];//!
		}
                else
                    qr->matrix->data[x][y] = bii[index];
                index++;
            }
        }
    }
    else
    {
        rmax = i-4;
        cmax = j+2;
	int index = 7;
        for (int x = i; x > rmax ; x--)
        {
            for (int y = j; y < cmax ; y++)
            {
                int m = qr->mask;
                int invert = 0;
                switch (m) {
                    case 0:  invert = (y + x) % 2 == 0;                    break;
                    case 1:  invert = x % 2 == 0;                          break;
                    case 2:  invert = y % 3 == 0;                          break;
                    case 3:  invert = (y + x) % 3 == 0;                    break;
                    case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                    case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                    case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                    case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
                }
                if(invert)
	        {
		    qr->matrix->data[x][y] = !bii[index]; //!
		}
		else
		    qr->matrix->data[x][y] = bii[index];
                index--;
            }
        }
    }
}
void insert_len(Qrcode * qr)
{
    int * biit = xcalloc(8,sizeof(int));
    bii(qr->message_len,biit);
    set_carreV(qr,biit,qr->matrix->rows-3,qr->matrix->columns-1,1);
}
void set_block(Qrcode qr,int * bin,int i, int j,int inclinaison, int direction)
{
    if (inclinaison == 0)
    {
        set_carreV(&qr,bin,i,j,direction);
    }
    else if (inclinaison == 1)
    {
        set_carreH(&qr,bin,i,j,direction);
    }
}

void insert_data(Qrcode * qr,int ** coordoner,int lencoor,char * message,int boolend)
{

    int len = 0;
    while(message[len] != '\0')
    {
        len++;
    }
    if(boolend)
    {
    	qr->message_len = len;
    	insert_len(qr);
    }
    int ** bin_data = xmalloc((lencoor + 1) * sizeof(void *));
    for(int i = 0; i< len; i++)
    {
        int * binvalue = xmalloc(9 * sizeof(int));
        bii((int)message[i],binvalue);
        bin_data[i] = binvalue;
    }

    int boool = 1;
    if(len<lencoor)
    {
	int * binvalue = calloc(8,sizeof(int));
	binvalue[4] = 1;
	binvalue[5] = 1;
	binvalue[6] = 1;
	bin_data[len] = binvalue;
    }
    for(int i = len+1; i<lencoor; i++)
    {
	int * binvalue = xmalloc(9 * sizeof(int));
	if(boool)
	{
		binvalue[0] = 1;
		binvalue[1] = 1;
		binvalue[7] = 1;
		boool = 0;
	}
	else
	{
		binvalue[3] = 1;
		binvalue[4] = 1;
		binvalue[5] = 1;
		binvalue[6] = 1;
		boool = 1;
	}
	bin_data[i] = binvalue;
    }
/*
    for (int i = 0; i<lencoor;i++)
    {
	printf(" ligne %d : ",i);
        for (int j = 0; j<8; j++)
        {
            printf("%d, ",bin_data[i][j]);
        }
        printf("\n");
    }
*/
    for (int i = 0; i< lencoor; i++)
    {
        set_block(*qr,bin_data[i],coordoner[i][0],coordoner[i][1],coordoner[i][2],coordoner[i][3]);
    }

    for(int i = 0; i<lencoor ; i++)
    {
	    free(bin_data[i]);
    }
    free(bin_data);
    qr->matrix->data[9][7] = 0;
    qr->matrix->data[10][7] = 1;


}


void insert_encoding(Qrcode * qr)
{
    int index = 0;
    char * mode = qr->encoding_mode;
    int biit[4] = {0,0,0,0};
    if (!strcmp("8-bit Byte",mode))
    {
        biit[1] = 1;
    }
    else if (!strcmp("Alphanumeric",mode))
    {
        biit[2] = 1;
    }
    else if (!strcmp("Numeric",mode))
    {
        biit[3] = 1;
    }
    for (int x = qr->matrix->rows-1; x > (int)qr->matrix->rows-3 ; x--)
    {
        for (int y = qr->matrix->columns-1; y > (int)qr->matrix->columns-3; y--)
        {
            int m = qr->mask;
            int invert = 0;
            switch (m) {
                case 0:  invert = (y + x) % 2 == 0;                    break;
                case 1:  invert = x % 2 == 0;                          break;
                case 2:  invert = y % 3 == 0;                          break;
                case 3:  invert = (y + x) % 3 == 0;                    break;
                case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
            }
            if(invert)
                qr->matrix->data[x][y] = !biit[index];
            else
                qr->matrix->data[x][y] = biit[index];
            index++;
        }
    }
}

Qrcode * init_qr(char *data)
{
/*
    int x;
    printf("enter a integer:\n");
    scanf("%d",&x);
    printf("Number = %d\n",x);
*/


    Qrcode * qr = xmalloc(sizeof(struct qrcode));

    qr->type = 1;
    qr->mask = 0;
    qr->encoding_mode = "8-bit Byte";
    qr->data = data;
    char *error = "aaaaaaa";
    //char *error = RS_encode_msg(data, 8);
    // note jai rentree : test qrunch find

    doubleMatrix *new_matrice = doubleMatrix_init_blocks(21,21);
    qr->matrix = new_matrice;


    int row = qr->matrix->rows;
    int col = qr->matrix->columns;

    int lati[17][4]={
    {row-7 ,col-1 ,0,1}, //0
    {row-11,col-1 ,1,1}, //1
    {row-7 ,col-4 ,0,0},
    {row-3 ,col-4 ,0,0},
    {row-1 ,col-3 ,1,0}, //4
    {row-3 ,col-5 ,0,1},
    {row-7 ,col-5 ,0,1},
    {row-11,col-5 ,1,1},
    {row-7 ,col-8 ,0,0},
    {row-3 ,col-8 ,0,0},
    {row-1 ,col-7 ,1,0},
    {row-3 ,col-9 ,0,1},
    {row-7 ,col-9 ,0,1},
    {row-11 ,col-9 ,0,1},
    {row-16 ,col-9 ,0,1},
    {1,col-9,1,1},
    {5,col-12,0,0}
    };
    int ende[7][4] = {
    {row-9,col-12,0,0}, //E1
    {row-5,col-12,0,0},
    {row-1,col-12,0,0},
    {row-9,col-13,0,1},
    {row-9,col-17,0,0},
    {row-9,col-18,0,1},
    {row-9,col-21,0,0}
    };


    int ** coordoner = xmalloc(17*sizeof(int*));
    for(int i = 0; i<17; i++)
    {
        coordoner[i] = xcalloc(4,sizeof(int));
        for (int j = 0; j<4;j++)
        {
            coordoner[i][j] = lati[i][j];
        }
    }
    int ** end = xmalloc(7*sizeof(int *));
    for(int i = 0; i<7; i++)
    {
        end[i] = xcalloc(4,sizeof(int));
        for (int j = 0; j<4;j++)
        {
            end[i][j] = ende[i][j];
        }
    }




    insert_encoding(qr);

    insert_mask(qr);

    insert_data(qr,coordoner,17,qr->data,1);

    insert_data(qr,end,7,error,0);

    for(int i = 0; i<7; i++)
    {
	    free(end[i]);
    }
    for(int i = 0 ; i<17 ; i++)
    {
	    free(coordoner[i]);
    }
    free(end);
    free(coordoner);

    return qr;
}
