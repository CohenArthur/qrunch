#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include "list.h"

struct link_list *new_ll(int size)
{
    //printf("new_ll \n");

    struct link_list *r = xmalloc(sizeof(struct link_list));

    r->data = size;

    struct link_list *index;
    struct link_list *stock = r;

    for(int i = 0; i < size; i++)
    {
        //printf("i = %d \n",i);

        index = xmalloc(sizeof(struct link_list));
        index->data = 0;
        index->next = NULL;

        stock->next = index;
        stock = index;       
    }

    //printf("Fin new_ll \n");

    return r;    
}

void print_ll(struct link_list *list)
{
    printf("Size = %d \n\n",list->data);

    list = list->next;

    while(list != NULL)
    {
        printf(" %d \n",list->data);
        list = list->next;        
    }    
}

void free_ll(struct link_list *list)
{
    struct link_list *stock;

    while(list != NULL)
    {
        stock = list->next;
        free(list);
        list = stock;        
    }    
}


