#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "xalloc.h"

#include "color.h"

#include "struct_qr.h"
#include "decode.h"
#include "encode.h"

#include "surface_to_image.h"

#include "matrix.h"
#include "image_treatment.h"

/*
 * Affiche l'aide relative aux options du programme './nogui'
 * Utilisation :
 *      print_help();
 */
void print_help();

/*
 * Recupere les arguments passes par ligne de commande et retourne le
 * nom du fichier demande
 *
 * Utilisation :
 *      int ma_verbose = 0;
 *      int mon_encode = 0;
 *      char *mon_fichier = get_args(argv, argc, &ma_verbose, &mon_encode);
 *      ...
 *      free(mon_fichier);
 */
char *get_args(int argv, char **argc, int *verbose, int *encode);

/*
 * Encode le message 'argument' et sauvegarde l'image en tant que qrunch.png
 */
void encode_cli(char *argument);

/*
 * Dechiffre l'image localisee en 'argument' et printf le resultat
 */
void decode_cli(char *argument, int verbose);

/*
 * Encode le message 'argument' et sauvegarde l'image en tant que qrunch.png
 */
char *encode_gui(char *argument, int nb);

/*
 * Dechiffre l'image et retourne la string associee
 */
char *decode_gui(char *argument);
