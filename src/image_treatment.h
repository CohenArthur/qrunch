#ifndef IMAGE_TREATMENT_H
#define IMAGE_TREATMENT_H

#include "xalloc.h"
#include <stdlib.h>
#include <SDL.h>
#include <err.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "image_treatment.h"
#include "matrix.h"
#include "sdl_functions.h"

#include "SDL/SDL_rotozoom.h"

#define PI 3.14159265

#include "matrix.h"
#include "sdl_functions.h"

/*
 * Recupere le pixel sur la surface 'surface' a la position 'width'*'height'
 */
Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height);

/*
 * Place un certain pixel 'pixel' a la position 'width'*'height' sur la Surface 'surface'
 */
void set_pixel(SDL_Surface *surface, unsigned width, unsigned height, Uint32 pixel);

/*
 * Inutile
 */
void update_surface(SDL_Surface *screen, SDL_Surface *image);

/*
 * Binarise la surface passee en argument
 */
void binarize(SDL_Surface *input);

/*
 * Retourne l'angle du QR Code present dans la surface 'input'
 *
 * @show : 0 ou 1, laisser a 0 pour utilisation normale
 */
int find_angle(SDL_Surface *input, int show);

/*
 * Nettoie l'image 'input' passee en parametre
 *
 * @show : 0 ou 1, laisser a 0 pour utilisation normale
 */
SDL_Surface *clean_image(SDL_Surface *input, int show);

/*
 * Supprime le fond d'une image
 *
 * @verbose : 0 ou 1, laisser a 0 pour utilisation normale
 */
SDL_Surface *remove_bg(SDL_Surface *input, int verbose);

/*
 * Convertis une surface image en une matrice simple
 *
 * @matrix : Checker matrix.h
 * @show : 0 ou 1, laisser a 0 pour utilisation normale
 */
matrix *image_to_matrix(SDL_Surface *input);

/*
 * Convertis une matrice double en une surface
 */
SDL_Surface *double_to_surface(doubleMatrix *matrix);

#endif
