#include "tdd.h"

void progress(size_t index, char *alert) {
	printf(RESET "%lu tests completed for %s\r", index, alert);
}

void success(char *alert) {
	printf(GRN "\nTest passed : %s\n" RESET, alert);
}

void assert_int(int left, int right, char *alert) {
	if (left != right)
		ALERT(alert);
}

void assert_long(long left, long right, char *alert) {
	if (left != right)
		ALERT(alert);
}

void assert_double(double left, double right, char *alert) {
	if (left != right)
		ALERT(alert);
}

void assert_string(char *left, char *right, char *alert) {
	if (strcmp(left, right) != 0)
		ALERT(alert);
}

void assert_char(char left, char right, char *alert){
	if (left != right)
		ALERT(alert);
}
