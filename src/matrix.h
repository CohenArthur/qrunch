#ifndef MATRIX_H
#define MATRIX_H

#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "xalloc.h"
//___________________________________________________________________________//
//______________________________MATRIX_______________________________________//
//___________________________________________________________________________//

/*
 * Initialiser la matrix avec matrix_init();
 * 		matrix *ma_matrice = matrix_init(x, y);
 * 		(ma_matrice->rows == x);
 * 		(ma_matrice->columns == y);
 *
 * Pour acceder aux donnees de la matrice :
 *		ma_matrice->data[index];
 */
typedef struct matrix
{
	unsigned rows;
	unsigned columns;
	int *data;
} matrix;

#define FOR_EACH(matrix, step) \
for (unsigned index_w = 0; index_w < matrix->rows; index_w = index_w + step) \
{ \
	for (unsigned index_h = 0; index_h < matrix->columns; index_h = index_h + step) \
	{ \

#define END_FE	\
	} \
} \

#define VERTICAL 0
#define HORIZONTAL 1

#define SET_TO_ONE dest_matrix->data[index_h][index_w] = 1

/*
 * Retourne une matrix allouee dynamiquement
 *
 * @rows : Le nombre de lignes de la matrice
 * @columns : Le nombre de colonnes de la matrice
 */
matrix *matrix_init(unsigned rows, unsigned columns);

/*
 * Supprime la matrice et sa data
 */
void matrix_free(matrix *matrix);

/*
 * Redimensionne une matrice en la reduisant le plus possible sans perdre
 * l'information contenue dans le QR Code
 */
matrix *matrix_resize(matrix *matrix);

/*
 * Affiche les informations de la matrice
 */
void print_matrixInfo(matrix *matrix);

/*
 * Affiche les donnees contenue dans la matrice
 */
void print_matrix(matrix *matrix);

/*
 * Affiche la matrice sous forme d'un tableau
 */
void print_matrix_array(matrix *matrix);

//___________________________________________________________________________//
//_____________________________DOUBLE_MATRIX_________________________________//
//___________________________________________________________________________//

/*
 * Pareil que 'matrix' sauf pour doubleMatrix->data
 * Acceder a la data se fait de la facon suivante :
 *		ma_double_matrice->data[index1][index2]
 */
typedef struct doubleMatrix
{
	unsigned rows;
	unsigned columns;
	int **data;
} doubleMatrix;

/*
 * Retourne une double matrice allouee
 */
doubleMatrix *doublematrix_init(unsigned rows, unsigned columns);

/*
 * Supprime la matrice de la memoire
 */
void doublematrix_free(doubleMatrix *doubleMatrix);

/*
 * Convertis une matrice simple en une matrice double
 */
doubleMatrix *simple_to_double(matrix *matrix);

/*
 * Redimensionne une matrice en augmentant sa taille d'un certain facteur.
 */
doubleMatrix *doublematrix_scale_up(doubleMatrix *matrix, size_t factor);

/*
 * Affiche une matrice double
 */
void print_double_matrix(doubleMatrix *doubleMatrix);

/*
 * Alloue une matrice double et met en place le motif des trois blocs
 */
doubleMatrix *doubleMatrix_init_blocks(unsigned rows, unsigned columns);

#endif
