#include "cl_args.h"

 void print_help(){
 	printf("DESCRIPTION : QRunch NoGUI - Dechiffrage et Encryptage de QRCodes\n\n");
	printf("UTILISATION : ./nogui [options] fichier\n\n");
	printf("OPTIONS :\n"GRN"-h ou --help : Affiche cet ecran\n\
-s ou --show : Affiche toutes les etapes du dechiffrage/encryptage\n\
-v ou --verbose : Pareil que \"-s\" ou \"--show\"\n\
-e ou --encode : Encode le message passe en argument\n" RESET);
}

//#include "file_operations.h"

char *get_args(int argv, char **argc, int *verbose, int *encode){
	*verbose = 0;
	char *filename = xcalloc(1, 256);

	if (argv > 3 || argv < 2){
		print_help();
		errx(1, RED "Nombre d'arguments incorrect" RESET);
	}
	if ((strcmp(*(argc + 1), "-h") == 0) || (strcmp(*(argc + 1), "--help") == 0)) {
			print_help();
			exit(0);
	}
	if (argv == 3){
		if ((strcmp(*(argc + 1), "-s") == 0) || (strcmp(*(argc + 1), "--show") == 0)\
		|| (strcmp(*(argc + 1), "-v") == 0) || (strcmp(*(argc + 1), "--verbose") == 0 ))
			*verbose = 1;
    if ((strcmp(*(argc + 1), "-e") == 0) || (strcmp(*(argc + 1), "--encode") == 0))
      *encode = 1;
		strcpy(filename, *(argc + 2));
	} else
		strcpy(filename, *(argc + 1));

	return filename;
}

void encode_cli(char *argument) {
    if(strlen(argument) > 17)
        errx(1, "Le message est trop long !");
    Qrcode *new_qr = init_qr(argument);
    new_qr->matrix = doublematrix_scale_up(new_qr->matrix, 20);

    SDL_Surface *result = double_to_surface(new_qr->matrix);
    surface_to_image(result, "output/qrunch.png");

    //FIXME Add free qrcode();

    SDL_FreeSurface(result);
    printf("Success : output/qrunch.png\n");
}

void decode_cli(char *argument, int verbose) {
    SDL_Surface *input_image = load_image(argument);

    matrix *simple = matrix_init((unsigned) input_image->w, (unsigned) input_image->h);
    simple = image_to_matrix(input_image);
    simple = matrix_resize(simple);

    doubleMatrix *input_matrix = simple_to_double(simple);

    Qrcode result;
    initqr(&result, input_matrix);

    printf("%s\n", result.data);

    if(verbose) {
        printf(CYN "QR CODE SPECS :\n\nEncoding Mode : %s\nType : %d\n\
Mask : %d\nMessage Length : %d\nData : %s\n\n" RESET,\
		result.encoding_mode, result.type, result.mask,\
		result.message_len,\
		result.data);
        print_double_matrix(result.matrix);
    }

    SDL_FreeSurface(input_image);
    matrix_free(simple);
    doublematrix_free(input_matrix);
}

char *encode_gui(char *argument, int nb){
    Qrcode *new_qr = init_qr(argument);
    new_qr->matrix = doublematrix_scale_up(new_qr->matrix, 20);
    char *filename = xcalloc(1, 256);
    char nb_str[12];

    sprintf(nb_str, "%d", nb);

    strcat(filename, "output/qrunch_");
    strcat(filename, nb_str);
    strcat(filename, ".png");
    SDL_Surface *result = double_to_surface(new_qr->matrix);
    surface_to_image(result, filename);

    //FIXME Add free qrcode();

    SDL_FreeSurface(result);

    return filename;
}

char *decode_gui(char *argument) {
    SDL_Surface *input_image = load_image(argument);

    matrix *simple = matrix_init((unsigned) input_image->w, (unsigned) input_image->h);
    simple = image_to_matrix(input_image);
    simple = matrix_resize(simple);

    doubleMatrix *input_matrix = simple_to_double(simple);

    Qrcode result;
    initqr(&result, input_matrix);

    SDL_FreeSurface(input_image);
    matrix_free(simple);
    doublematrix_free(input_matrix);

    return(result.data);
}
