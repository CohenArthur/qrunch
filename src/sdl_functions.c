#include "sdl_functions.h"

void init_sdl(){
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
		errx(1, "Couldn't initialize SDL: %s\n", SDL_GetError());
}

SDL_Surface *load_image(char *path){
	SDL_Surface *img = IMG_Load(path);

	if (!img)
		errx(3, "Couldn't load %s: %s\n", path, IMG_GetError());

	return img;
}

SDL_Surface *display_image(SDL_Surface *img){
	SDL_Surface *screen;

	screen = SDL_SetVideoMode(img->w, img->h, 0, SDL_SWSURFACE|SDL_ANYFORMAT);

	if (screen == NULL)
		errx(1, "Couldn't set %dx%d video mode: %s\n",
				img->w, img->h, SDL_GetError());

	if(SDL_BlitSurface(img, NULL, screen, NULL) < 0)
		warnx("BlitSurface error: %s\n", SDL_GetError());

	SDL_UpdateRect(screen, 0, 0, img->w, img->h);
	return screen;
}

SDL_Surface* crop_surface(SDL_Surface* input_image, int x, int y, int width, int height){
	SDL_Surface* result_surface = SDL_CreateRGBSurface(input_image->flags, width, height, input_image->format->BitsPerPixel, input_image->format->Rmask, input_image->format->Gmask, input_image->format->Bmask, 0x000000ff);

    SDL_Rect rect = {x, y, width, height};
    SDL_BlitSurface(input_image, &rect, result_surface, NULL);

    return result_surface;
}

void wait_for_keypressed(){
	SDL_Event event;

	do{
		SDL_PollEvent(&event);
	}while (event.type != SDL_KEYDOWN);

	do{
		SDL_PollEvent(&event);
	}while (event.type != SDL_KEYUP);
}
