#include <stdio.h>
#include <stdlib.h>
#include "decode.h"
#include <stdio.h>

int get_mask(Qrcode *qr)
{

    int un = qr->matrix->data[8][2];
    int deux = qr->matrix->data[8][3];
    int trois = qr->matrix->data[8][4];

    int sum = (4*un)+(2*deux)+(trois*1);
    switch(sum)
    {
        case 0: return 5;
        case 1: return 4;
        case 2: return 7;
        case 3: return 6;
        case 4: return 1;
        case 5: return 0;
        case 6: return 3;
        case 7: return 2;
    }
    /*
       if (qr->matrix->data[8][2] == 1)
       {
       if (qr->matrix->data[8][3] == 1)
       {
       if (qr->matrix->data[8][4] == 1)
       {
       return 7;
       }
       else
       {
       return 6;
       }
       }
       else
       {
       if (qr->matrix->data[8][4] == 1)
       {
       return 5;
       }
       else
       {
       return 4;
       }

       }

       }
       else
       {
       if (qr->matrix->data[8][3] == 1)
       {
       if (qr->matrix->data[8][4] == 1)
       {
       return 3;
       }
       else
       {
       return 2;
       }
       }
       else
       {
       if (qr->matrix->data[8][4] == 1)
       {
       return 1;
       }
       else
       {
       return 0;
       }
       }

       }
       */
    return 0;
}

void get_encoding_mode(Qrcode *qr, char** p)
{
    int data = 1;
    for (int x = qr->matrix->rows-1; x > (int)qr->matrix->rows-3 ; x--)
    {
        for (int y = qr->matrix->columns-1; y > (int)qr->matrix->columns-3; y--)
        {
            int m = qr->mask;
            int invert = 0;
            switch (m) {
                case 0:  invert = (y + x) % 2 == 0;                    break;
                case 1:  invert = x % 2 == 0;                          break;
                case 2:  invert = y % 3 == 0;                          break;
                case 3:  invert = (y + x) % 3 == 0;                    break;
                case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
            }
            int noire = 1;
            if(invert)
                noire = 0;
            data *= 10;
            if(qr->matrix->data[x][y] == noire)
            {
                data++;
            }
        }
    }
    //printf("Test Data = %d \n",data);
    if (data == 10100 )
        p[0]= "8-bit Byte";
    else if (data == 10010)
        p[0]= "Alphanumeric";
    else if (data == 10001)
        p[0]= "Numeric";
    else
        p[0] = "Encoding not detected...";
    if(!p)
    {
        printf("Relou les warning");
    }

    //printf("Test P = %s\n", p[0]);

}

int get_message_len(Qrcode *qr)
{
    //int puissance[8] = {128,64,32,16,8,4,2,1};
    int puissanceinv[8] = {1,2,4,8,16,32,64,128};
    int data = 0;
    int pui = 0;
    for (int x = qr->matrix->rows-6; x < (int)qr->matrix->rows-2; x++)
    {
        for (int y = qr->matrix->columns-2; y < (int)qr->matrix->columns; y++)
        {
            int m = qr->mask;
            int invert = 0;
            switch (m) {
                case 0:  invert = (y + x) % 2 == 0;                    break;
                case 1:  invert = x % 2 == 0;                          break;
                case 2:  invert = y % 3 == 0;                          break;
                case 3:  invert = (y + x) % 3 == 0;                    break;
                case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
            }
            int noire = 1;
            if(invert)
                noire = 0;
            if(qr->matrix->data[x][y] == noire)
            {
                data+= puissanceinv[pui];
            }
            pui++;

        }
    }
    //printf("Message Len Data = %d \n",data);
    return data;

}
int get_bitmask(Qrcode *qr,int x,int y,int bit)
{
    int m = qr->mask;
    int invert = 0;
    switch (m) {
        case 0:  invert = (y + x) % 2 == 0;                    break;
        case 1:  invert = x % 2 == 0;                          break;
        case 2:  invert = y % 3 == 0;                          break;
        case 3:  invert = (y + x) % 3 == 0;                    break;
        case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
        case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
        case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
        case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
    }
    if (invert)
    {
        if(bit)
        {
            return 0;
        }
        return 1;
    }
    return bit;
}
char get_carreH(Qrcode *qr, int i, int j,int direction)
{
    //int puissance[8] = {1,2,4,8,16,32,64,128};
    int puissanceinv[8] = {128,64,32,16,8,4,2,1};
    int data = 0;
    int pui = 0;
    if(direction)
    {
        int * textinv = xcalloc(8,sizeof(int));
        textinv[0] = get_bitmask(qr,i,j,qr->matrix->data[i][j]);
        textinv[1] = get_bitmask(qr,i,j-1,qr->matrix->data[i][j-1]);
        textinv[2] = get_bitmask(qr,i-1,j,qr->matrix->data[i-1][j]);
        textinv[3] = get_bitmask(qr,i-1,j-1,qr->matrix->data[i-1][j-1]);
        textinv[4] = get_bitmask(qr,i-1,j-2,qr->matrix->data[i-1][j-2]);
        textinv[5] = get_bitmask(qr,i-1,j-3,qr->matrix->data[i-1][j-3]);
        textinv[6] = get_bitmask(qr,i,j-2,qr->matrix->data[i][j-2]);
        textinv[7] = get_bitmask(qr,i,j-3,qr->matrix->data[i][j-3]);

        for (int i = 0;i<8;i++)
        {
            //printf("%d\n",textinv[i]);
            if(textinv[i])
                data += puissanceinv[pui];
            pui++;
        }
        //printf("dataH = %d\n",(data-1));

        return (char) data;

    }
    // int rmax = 0;
    // int cmax = 0;
    // rmax = i-2;
    // cmax = j-4;
    int * textinv = xcalloc(8,sizeof(int));
    textinv[2] = get_bitmask(qr,i,j,qr->matrix->data[i][j]);
    textinv[3] = get_bitmask(qr,i,j-1,qr->matrix->data[i][j-1]);
    textinv[0] = get_bitmask(qr,i-1,j,qr->matrix->data[i-1][j]);
    textinv[1] = get_bitmask(qr,i-1,j-1,qr->matrix->data[i-1][j-1]);
    textinv[6] = get_bitmask(qr,i-1,j-2,qr->matrix->data[i-1][j-2]);
    textinv[7] = get_bitmask(qr,i-1,j-3,qr->matrix->data[i-1][j-3]);
    textinv[4] = get_bitmask(qr,i,j-2,qr->matrix->data[i][j-2]);
    textinv[5] = get_bitmask(qr,i,j-3,qr->matrix->data[i][j-3]);

    for (int i = 0;i<8;i++)
    {
        //printf("%d\n",textinv[i]);
        if(textinv[i])
            data += puissanceinv[pui];
        pui++;
    }
    //printf("dataH = %d\n",(data-1));

    return (char) data;


    /*
       for (int y = cmax; y < j ; y++)
       {
       for (int x = rmax; x < i ; x++)
       {
       int m = qr->mask;
       int invert = 0;
       switch (m) {
       case 0:  invert = (y + x) % 2 == 0;                    break;
       case 1:  invert = x % 2 == 0;                          break;
       case 2:  invert = y % 3 == 0;                          break;
       case 3:  invert = (y + x) % 3 == 0;                    break;
       case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
       case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
       case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
       case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
       }
       int noire = 1;
       if(invert)
       noire = 0;

       if(qr->matrix->data[x][y] == noire)
       {
       data+= puissance[pui];
       printf("1");
       }
       else
       printf("0");
       pui++;
       }
       }
       printf("HHHHH = %d\n", data);
       return (char) data-1;*/
}

char get_carreV(Qrcode *qr,int i, int j,int direction)
{
    int puissance[8] = {1,2,4,8,16,32,64,128};
    int puissanceinv[8] = {128,64,32,16,8,4,2,1};
    int data = 0;
    int pui = 0;

    int rmax = 0;
    int cmax = 0;

    if(direction){
        rmax = i-4;
        cmax = j-2;

        for (int x = i; x > rmax ; x--)
        {
            for (int y = j; y > cmax ; y--)
            {
                int m = qr->mask;
                int invert = 0;
                switch (m) {
                    case 0:  invert = (y + x) % 2 == 0;                    break;
                    case 1:  invert = x % 2 == 0;                          break;
                    case 2:  invert = y % 3 == 0;                          break;
                    case 3:  invert = (y + x) % 3 == 0;                    break;
                    case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                    case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                    case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                    case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
                }
                int noire = 1;
                if(invert)
                    noire = 0;

                if(qr->matrix->data[x][y] == noire)
                {
                    if(direction)
                        data+= puissanceinv[pui];
                    else
                        data+= puissance[pui];
                }
                pui++;

            }
        }
    }
    else
    {
        rmax = i-4;
        cmax = j+2;

        for (int x = i; x > rmax ; x--)
        {
            for (int y = j; y < cmax ; y++)
            {
                int m = qr->mask;
                int invert = 0;
                switch (m) {
                    case 0:  invert = (y + x) % 2 == 0;                    break;
                    case 1:  invert = x % 2 == 0;                          break;
                    case 2:  invert = y % 3 == 0;                          break;
                    case 3:  invert = (y + x) % 3 == 0;                    break;
                    case 4:  invert = (y / 3 + x / 2) % 2 == 0;            break;
                    case 5:  invert = y * x % 2 + y * x % 3 == 0;          break;
                    case 6:  invert = (y * x % 2 + y * x % 3) % 2 == 0;    break;
                    case 7:  invert = ((y + x) % 2 + y * x % 3) % 2 == 0;  break;
                }
                int noire = 1;
                if(invert)
                    noire = 0;

                if(qr->matrix->data[x][y] == noire)
                {
                    if(direction)
                        data+= puissanceinv[pui];
                    else
                        data+= puissance[pui];
                }
                pui++;

            }
        }

    }
    //printf("dataV = %d n",data);
    return (char) data;

}
char get_carre(Qrcode *qr,int i, int j,int inclinaison,int direction)
{
    char ret = 0;

    if (inclinaison == 0)
    {
        return get_carreV(qr,i,j,direction);
    }
    else if (inclinaison == 1)
    {
        return get_carreH(qr,i,j,direction);
    }

    return ret; //ATTENTION //FIXME
}

char * get_data(Qrcode *qr)
{
    int row = qr->matrix->rows;
    int col = qr->matrix->columns;

    if(qr->type == 2)
    {
        int lati[21][4]={
            {row-7 ,col-1 ,0,1},
            {row-11 ,col-1 ,0,1},
            {row-15 ,col-1 ,0,1},
            {row-19,col-1 ,1,1},
            {row-15 ,col-4 ,0,0},
            {row-11,col-4 ,0,0},
            {row-7,col-4 ,0,0},
            {row-3,col-4 ,0,0},
            {row-1 ,col-3 ,1,0},
            {row-3 ,col-5 ,0,1},
            {row-7 ,col-5 ,0,1},
            {row-11,col-5 ,1,1},
            {row-7 ,col-8 ,0,0},
            {row-3 ,col-8 ,0,0},
            {row-1 ,col-7 ,1,0},
            {row-3 ,col-9 ,0,1},
            {row-7 ,col-9 ,0,1},
            {row-11 ,col-9 ,0,1},
            {row-16 ,col-9 ,0,1},
            {1,col-9,1,1},
            {5,col-12,0,0}};
        char* data = xcalloc(qr->message_len, sizeof(char));
        for (int i = 0; i < qr->message_len ; i++)
        {
            data[i] =get_carre(qr,lati[i][0],lati[i][1],lati[i][2],lati[i][3]);
            printf("datai %c\n",data[i]);
        }
        return data;
        //{18 ,col-1 ,0,1},
    }
    else
    {
        int lati[17][4]={
            {row-7 ,col-1 ,0,1},
            {row-11,col-1 ,1,1},
            {row-7 ,col-4 ,0,0},
            {row-3 ,col-4 ,0,0},
            {row-1 ,col-3 ,1,0},
            {row-3 ,col-5 ,0,1},
            {row-7 ,col-5 ,0,1},
            {row-11,col-5 ,1,1},
            {row-7 ,col-8 ,0,0},
            {row-3 ,col-8 ,0,0},
            {row-1 ,col-7 ,1,0},
            {row-3 ,col-9 ,0,1},
            {row-7 ,col-9 ,0,1},
            {row-11 ,col-9 ,0,1},
            {row-16 ,col-9 ,0,1},
            {1,col-9,1,1},
            {5,col-12,0,0}};
        char* data = xcalloc(qr->message_len, sizeof(char));
        for (int i = 0; i < qr->message_len ; i++)
        {
            data[i] =get_carre(qr,lati[i][0],lati[i][1],lati[i][2],lati[i][3]);
        }
        return data;

    }
    //char *buffer = xmalloc(100 * sizeof(char));
    //
    /*
       buffer[0]=get_carre(qr,qr->matrix->rows - 7,qr->matrix->columns-1,0,1);

       buffer[1]=get_carre(qr,qr->matrix->rows - 11,qr->matrix->columns-1,1,1);

       buffer[2]=get_carre(qr,qr->matrix->rows - 7,qr->matrix->columns-4,0,0);
       buffer[3]=get_carre(qr,qr->matrix->rows - 3,qr->matrix->columns-4,0,0);

       buffer[4]=get_carre(qr,qr->matrix->rows - 2,qr->matrix->columns-3,1,1);

       buffer[5]=get_carre(qr,qr->matrix->rows - 3,qr->matrix->columns-5,0,1);
       buffer[6]=get_carre(qr,qr->matrix->rows - 7,qr->matrix->columns-5,0,1);

       buffer[7]=get_carre(qr,qr->matrix->rows - 11,qr->matrix->columns-5,1,1);

       buffer[8]=get_carre(qr,qr->matrix->rows - 7,qr->matrix->columns-8,0,0);
       buffer[9]=get_carre(qr,qr->matrix->rows - 3,qr->matrix->columns-8,0,0);
       */
    //free(buffer);
}
