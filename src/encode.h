#ifndef ENCODE_H

#define ENCODE_H

#include "struct_qr.h"

#include "xalloc.h"

void bii(unsigned int n,int * binvalue);

/*
 * Ajoute un mask au QRCode
 */
void insert_mask(Qrcode *qr);

/*
 * Ajoute la longueur du message au QRCode
 */
void insert_len(Qrcode * qr);

/*
 * Remplis le bitmask du QRCode
 */
int set_bitmask(Qrcode *qr, int x,int y,int bit);

void set_carreH(Qrcode *qr, int * bit, int i, int j, int direction);
void set_carreV(Qrcode *qr, int * bii, int i, int j, int direction);
void set_block(Qrcode qr, int * bin, int i, int j,int inclinaison,int direction);

/*
 * Insere la data dans le QRCode
 */
void insert_data(Qrcode * qr, int ** coordoner,int lencoor,char *message,int boolend);

/*
 * Retourne QRCode initialise correctement avec la data 'data'
 */
Qrcode * init_qr(char *data);

#endif
