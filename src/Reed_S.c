#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include "Reed_S.h"

#include "GFb.h"
#include "poly.h"

/*int qr_check_format(format)
{
    g = 10100110111;

    for(i = 4; i >= 0; i--)
    {
        //if
    }

    return 42;
}

int RS_decode_msg(int m, int n,  int k, int t, *char c)
{

    if(k + 2*t != n)
    {
        errx(,"Reed-Solomon : invalid k and t");
    }

    if(power(2,m) - 1 != n)
    {
        errx(,"Reed-Solomon : invalid m");
    }

    // Tester si len(c) = n

    char[] red = c; // c de k+1 a 2*t

    char[] data = c; // c de 0 a k

    struct Poly G;

    struct Poly A;

    struct Poly B;

    struct Poly C;

    struct Poly E;




    return 42;
}*/


struct poly *RS_generator_poly(int nsym)

/*Generate an irreducible generator polynomial
(necessary to encode a message into Reed-Solomon)*/

{
    struct poly *a = new_poly(0);

    gf_changeval_poly(a, 0, 1);

    for(int i = 0; i < nsym; i++)
    {
        struct poly *b = new_poly(1);

        gf_changeval_poly(b, 1, 1);
        gf_changeval_poly(b, 0, gf_pow(2, i));

        a = gf_poly_mult(a,b);

    }

    
    return a;
}


char *RS_encode_msg(char *msg_in, int nsym)

/*Reed-Solomon main encoding function,
using polynomial division (algorithm Extended Synthetic Division)*/

{

    char *c = msg_in;
    int len_in = 0;
    int len_out = nsym;

    while(c[len_in] != '\0')
    {
        len_in++;
    }

    len_out += len_in;

    if((len_out) > 255)
    {
        err(42,"Invalid message size, must be <= 255");
    }

    struct poly *gen = RS_generator_poly(nsym);

    int len_x = len_in + gen->degree;

    char *msg_out = xmalloc(sizeof(char)*len_x);

    for(int i = 0; i < len_in; i++)
    {
        msg_out[i] = msg_in[i];
    }

    for(int i = len_in; i < len_x; i++)
    {
        msg_out[i] = 0;
    }

    for(int i = 0; i < len_in; i++)
    {
        char coef = msg_out[i];

        if(coef != 0)
        {
            for(int j = 1; j < gen->degree; j++)
            {
                msg_out[i+j] ^= gf_mult(gf_getval_poly(gen, j), coef);
            }
        }
    }

    for(int i = 0; i < len_in; i++)
    {
        msg_out[i] = msg_in[i];
    }

    return msg_out;
}

int *RS_encode_format(char *format_in)
{
    char *msg_out = RS_encode_msg(format_in, 3);

    static int format_out[2];

    int n = 2;

    while(msg_out[n] != '\0')
    {
        format_out[n-2] = (int)msg_out[n];
        ++n;
    }

    return format_out;
}
