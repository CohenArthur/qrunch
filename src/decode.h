#ifndef DECODE_H
#define DECODE_H
#include "struct_qr.h"

#include "xalloc.h"
/*
 * Retourne le mask du QRCode
 */
int get_mask(Qrcode *qr);

/*
 * Retourne le mode d'encodage
 */
void get_encoding_mode(Qrcode *qr,char** p);

/*
 * Retourne la longueur du message chiffre
 */
int get_message_len(Qrcode *qr);

/*
 * Retourne le message chiffre
 */
char * get_data(Qrcode *qr);






#endif
