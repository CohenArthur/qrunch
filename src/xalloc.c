#include "xalloc.h"

void *xmalloc(size_t size){
    void *pointy = NULL;
    pointy = malloc(size);
    if(!pointy)
        errx(1, "xmalloc() failed");
    return pointy;
}

void *xcalloc(size_t qty, size_t size){
    void *pointy = NULL;
    pointy = calloc(qty, size);
    if(!pointy)
        errx(1, "xcalloc() failed");
    return pointy;
}
