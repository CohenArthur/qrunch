#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include "poly.h"
#include "list.h"
#include "GFb.h"

struct poly *new_poly(int degree)
{
    struct poly *r = xmalloc(sizeof(struct poly));

    r->degree = degree;

    struct link_list *coef = new_ll(degree + 1);

    r->coef = coef;

    return r;
}

void gf_print_poly(struct poly *p)
{
    printf("Polynomial of degree %d \n\n", p->degree);

    print_ll(p->coef);    
    
}

int gf_getval_poly(struct poly *p, int pos)
{
    struct link_list *coef = p->coef;

    coef = coef->next;

    while(coef != NULL && pos > 0)
    {
        coef = coef->next;
        --pos;        
    }

    if(coef != NULL)
    {
        return coef->data;    
    }

    return 0;
}

void gf_changeval_poly(struct poly *p, int pos, int val)
{
    struct link_list *coef = p->coef;

    coef = coef->next;

    while(coef != NULL && pos > 0)
    {
        coef = coef->next;
        --pos;
    }

    if(coef != NULL)
    {
        coef->data = val;        
    }   
}

/*struct poly *gf_poly_scalarmult(struct poly *p, uint8_t d)
{
    struct poly *r = xmalloc(sizeof(struct poly));

    for( int i = 0, i < p->degree, i++)
    {
        r->coef[i] = gf_mult(p->coef[i], d);
    }

    return r;
}

struct poly *gf_poly_add(struct poly *p1, struct poly *p2)
{
    struct poly *r = xmalloc(sizeof(struct poly));

    for(int i = 0, )
    {
        r->coef[] = p->coef[i]    
    }

    for()
    {
        r->coef[]
    }

    return ;    
}*/

struct poly *gf_poly_mult(struct poly *p, struct poly *q)

/*Multiply two polynomials, inside Galois Field */

{
    struct poly *r = new_poly(p->degree + q->degree);

    for(int i = 0; i <= p->degree; ++i)
    {
        for(int j = 0; j <= q->degree; ++j)
        {
            gf_changeval_poly(r, i + j, gf_add(gf_getval_poly(r, i + j), gf_mult(gf_getval_poly(p, i), gf_getval_poly(q, j))));
            
        }
    }

    return r;
}

/*struct poly *gf_poly_div(struct poly *dividend, struct poly *divisor, struct poly *remain)

Fast polynomial division by using Extended Synthetic Division and optimized for GF(2^p) computations

{
    d_divd = dividend->degree;
    d_divs = divisor->degree;

    len = d_divd-d_divs;


    struct poly *out = new_poly(d_divd);

    for(int i = 0; i < d_divd; i++)
    {
        out->coef[i] = dividend->coef[i];    
        
    }

    for(int i = 0; i < len; i++)
    {
        int coef = result->coef[i];

        if(coef !=0)
        {
            for(int j = 1; d_divs)
            {
                if(divisor->coef[j] != 0)
                {
                    result->coef[i + j] = gf_mult(divisor[j], coef);    
                }
            }
        }
    }

    int separator = -(d_divs - 1);

    struct poly *out = new_poly(separator);

    for(int i = 0; i < separator; i++)
    {
        result->coef[i] = out[i];    
    }

    for(int i = separator; i < d_divd; i++)
    {
        remainder->coef[i] = out->coef[separator];    
    }

    return result;
}

uint8_t gf_eval_poly(struct poly *p, uint8_t x)
{
    uint8_t r = p->coef[0];

    for(int i = 1, i < p->degree, i++)
    {
        r = gf_mult(p->coef[i], x) ^ y; //FIX IT
    }

    return r;
}

struct poly *find_prime_polys(int generator, int c_exp, bool fast_prime, ,bool single)
{

    struct poly *prime_poly = xmalloc(sizeof(struct poly));
    
    return prime_poly;
}*/

void free_poly(struct poly *poly)
{
    free_ll(poly->coef);
    free(poly);
}




