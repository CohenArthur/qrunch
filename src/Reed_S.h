#ifndef REED_S_H
#define REED_S_H

#include "xalloc.h"

struct poly *RS_generator_poly(int nsym);

char *RS_encode_msg(char *msg_in, int nsym);

int *RS_encode_format(char *format_in);

#endif
