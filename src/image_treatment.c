#include "image_treatment.h"

int absolute_value(int n) {
	if (n < 0)
		return -n;
	return n;
}

static inline
Uint8* pixel_ref(SDL_Surface *surface, unsigned x, unsigned y) {
	int bpp = surface->format->BytesPerPixel;
	return (Uint8*)surface->pixels + y * surface->pitch + x * bpp;
}

Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height) {
	Uint8 *p = pixel_ref(surface, width, height);

	switch(surface->format->BytesPerPixel){
		case 1:
			return *p;
		case 2:
			return *(Uint16 *)p;
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		case 4:
			return *(Uint32 *)p;
	}
		return 0;
}

void set_pixel(SDL_Surface *surface, unsigned height, unsigned width, Uint32 pixel) {
    Uint8 *p = pixel_ref(surface, height, width);

    switch(surface->format->BytesPerPixel){
        case 1:
            *p = pixel;
            break;
        case 2:
            *(Uint16 *)p = pixel;
            break;
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN){
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }else{
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;
        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void update_surface(SDL_Surface *screen, SDL_Surface *image) {
	if (SDL_BlitSurface(image, NULL, screen, NULL) < 0)
		warnx("BlitSurface error: %s Refreshing surface failed.\n", SDL_GetError());

	SDL_UpdateRect(screen, 0, 0, image->w, image->h);
}

void binarize(SDL_Surface *input) {
	size_t width	 = input->w;
	size_t height	 = input->h;

	for (size_t index_w = 0; index_w < width; index_w++) {
		for (size_t index_h = 0; index_h < height; index_h++) {

			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);

			if (r + g + b < 100) {
				r = 0;
				g = 0;
				b = 0;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			} else {
				r = 255;
				g = 255;
				b = 255;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			}
		}
	}
}

int find_angle(SDL_Surface *input, int show) {
 	double angle;

	ssize_t width  = input->w;
	ssize_t height = input->h;

	int fp_found = 0; //First Point bool and coordinates
	ssize_t fp_w = 0;
	ssize_t fp_h = 0;

	ssize_t sp_w = 0; //Second point coordinates
	ssize_t sp_h = 0;

	ssize_t pp_w = 0; //Previous Point Coordinates
	ssize_t pp_h = 0; //Previous Point Coordinates

	for (ssize_t index_h = 0; index_h < height; index_h++) {
		for (ssize_t index_w = 0; index_w < width; index_w++) {

			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);

			if (r + g + b == 0 && !fp_found) {
				if (show) {
					r = 255;
					g = 0;
					b = 0;
					printf("\nFound first point\nHeight Coordinate : %lu\n\
Width Coordinate : %lu\n", index_h, index_w);
					pixel = SDL_MapRGB(input->format, r, g, b);
					set_pixel(input, index_w, index_h, pixel);
				}
				fp_found = 1;
				fp_w = index_w;
				fp_h = index_h;
				pp_w = fp_w;
				pp_h = fp_h;
				index_w = 0;
				index_h++;
			} else if (r + g + b == 0 && fp_found){
				if (index_w >= pp_w && (index_h - pp_h) > 1) {
					if (show) {
						r = 0;
						g = 0;
						b = 255;
						printf("\nFound second point\nHeight Coordinate : %lu\n\
Width Coordinate : %lu\n\n", index_h, index_w);
						pixel = SDL_MapRGB(input->format, r, g, b);
						set_pixel(input, index_w, index_h, pixel);
					}
					sp_w = index_w;
					sp_h = index_h;
				index_w = width + 1;
					index_h = height + 1;
				} else {
					pp_w = index_w;
					index_w = 0;
					index_h++;
				}
			} else {
				if (show) {
					r = 0;
					g = 255;
					b = 0;
					pixel = SDL_MapRGB(input->format, r, g, b);
					set_pixel(input, index_w, index_h, pixel);
				}
			}
		}
	}

	angle = (atan((double)(sp_h - fp_h) / (double)(sp_w - fp_w)) * 180/PI) + 0.80;
	if ((int)angle % 90 == 0)
			return 0;
	return (int) angle;
}

SDL_Surface *clean_image(SDL_Surface *input, int show) {

	size_t width = input->w;
	size_t height = input->h;

	size_t min = height;

	int factor = 0;

	show = show;
	//Uint8 r, g, b;

	if (width < height)
		min = width;

	for (size_t index = 0; index < min/2; index++) {
		Uint32 pixel = get_pixel(input, index, index);
		Uint8 r_tl, g_tl, b_tl;
		SDL_GetRGB(pixel, input->format, &r_tl, &g_tl, &b_tl);

		if (r_tl == 255) {
			pixel = get_pixel(input, width - index, index);
			Uint8 r_tr, g_tr, b_tr;
			SDL_GetRGB(pixel, input->format, &r_tr, &g_tr, &b_tr);

			pixel = get_pixel(input, index, height - index);
			Uint8 r_bl, g_bl, b_bl;
			SDL_GetRGB(pixel, input->format, &r_bl, &g_bl, &b_bl);

			pixel = get_pixel(input, width - index, height - index);
			Uint8 r_br, g_br, b_br;
			SDL_GetRGB(pixel, input->format, &r_br, &g_br, &b_br);

			if (r_tl == 255 && r_tr == 255 && r_bl == 255 && r_br == 255) {
				factor = (int) index;
				break;
			}

		}

		//Add error gestion when not finding white pixels
/*
		if (show == 1) {
			r = 255;
			g = 0;
			b = 0;
			pixel = SDL_MapRGB(input->format, r, g, b);
			set_pixel(input, index, index, pixel);
			set_pixel(input, width - index, index, pixel);
			set_pixel(input, index, height - index, pixel);
			set_pixel(input, width - index, height - index, pixel);
		}
		*/
	}

	SDL_Surface *result_crop = NULL;

	result_crop = crop_surface(input, factor, factor, width - (2 * factor), height - (2 * factor));
	return result_crop;
}

SDL_Surface *remove_bg(SDL_Surface *input, int verbose) {
	SDL_Surface *result;

	size_t width  = input->w;
	size_t height = input->h;

	size_t index_w = 0;
	size_t index_h = 0;

	size_t top_left_w = 0;
	size_t top_left_h = 0;
	size_t bot_right_w = 0;
	size_t bot_right_h = 0;

	for (index_w = 0; index_w < width; index_w++) {
		for (index_h = 0; index_h < height; index_h++) {
			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);
			if (r + g + b == 0) {
				top_left_w = index_w;
				top_left_h = index_h;
				goto loop_top_end;
			}
		}
	}

	loop_top_end:

	for (index_w = width - 1; index_w > 0; index_w--) {
		for (index_h = width - 1; index_h > 0; index_h--) {
			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);
			if (r + g + b == 0){
				bot_right_w = index_w;
				bot_right_h = index_h;
				goto loop_bot_end;
			}
		}
	}

	loop_bot_end:

	if(verbose)
		printf("\nDEBUG :\ntop_left_w = %lu\ntop_left_h = %lu\n\
bot_right_w = %lu\nbot_right_h = %lu\n",\
							top_left_w, top_left_h, bot_right_w, bot_right_h);

	result = crop_surface(input, top_left_w, top_left_h,\
						 	absolute_value(bot_right_w - top_left_w),
							absolute_value(bot_right_h - top_left_h));\

	return result;
}

matrix *image_to_matrix(SDL_Surface *input){
	int width  = input->w;
	int height = input->h;

	matrix *new_matrix;
	new_matrix = matrix_init((unsigned)width, (unsigned)height);

	for (int index_w = 0; index_w < width; index_w++) {
		for (int index_h = 0; index_h < height; index_h++) {
			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);

			if (r + g + b > 0)
				continue;
			else
				new_matrix->data[index_w + index_h * width] = 1;
		}
	}
	return new_matrix;
}

SDL_Surface *double_to_surface(doubleMatrix *matrix){
	SDL_Surface *output_surface = NULL;

	output_surface = SDL_CreateRGBSurface(0, matrix->rows, matrix->columns, 32,\
										0, 0, 0, 0);

	Uint32 black_pixel = SDL_MapRGB(output_surface->format, 0, 0, 0);
	Uint32 white_pixel = SDL_MapRGB(output_surface->format, 255, 255, 255); //FIXME

	FOR_EACH(matrix, 1)
		if (matrix->data[index_w][index_h] != 1)
			set_pixel(output_surface, index_w, index_h, white_pixel);
		else
			set_pixel(output_surface, index_w, index_h, black_pixel);
	END_FE;

	return output_surface;
}
