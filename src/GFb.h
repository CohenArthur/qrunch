#ifndef GF_H
#define GF_H

#include <stdio.h>
#include <stdlib.h>

#include "xalloc.h"

void init_tables();

void print_tables(int b_exp, int b_log);

u_int8_t gf_add(u_int8_t x, u_int8_t y);

u_int8_t gf_sub(u_int8_t x, u_int8_t y);

u_int8_t gf_mult(u_int8_t x, u_int8_t y);

u_int8_t gf_pow(u_int8_t x, u_int8_t power);

#endif
