#ifndef LIST_H
#define LIST_H

#include "xalloc.h"

struct link_list
{
    struct link_list *next;
    int data;
};

struct link_list *new_ll(int size);

void print_ll(struct link_list *list);

void free_ll(struct link_list *list);

#endif
