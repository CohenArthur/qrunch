#include "src/sdl_functions.h"
#include "src/image_treatment.h"
#include "err.h"

#include "src/struct_qr.h"

#include "SDL/SDL_rotozoom.h"

#include "src/color.h"

#include "src/tdd.h"

#include "src/surface_to_image.h"
#include "src/matrix.h"

#include "src/list.h"
#include "src/GFb.h"
#include "src/poly.h"
#include "src/Reed_S.h"

#include "src/xalloc.h"

void test_xalloc(){
    char *alert = "test_xalloc()";

    int *x = xmalloc(sizeof(int));
    *x = 27;
    assert_int(*x, 27, alert);
    free(x);

    success(alert);
}

void test_error_correction(){

    char *alert = "test_error_correction()";

    init_tables();

    printf("INIT SUCCESS \n \n");

    print_tables(1, 1);

    u_int8_t test = 42;

    printf("test init = %d \n", test);

    test = gf_mult(137/*10001001*/, 42/*00101010*/);

    printf("test mult 108 * 3 = %d \n", test);

    test = gf_pow(2, 8);

    printf("test pow 2^8 = %d \n", test);

    test = gf_pow(2, 3);

    printf("test pow 2^3 = %d \n", test);

    test = gf_pow(2, 2);

    printf("test pow 2^2 = %d \n", test);

    test = gf_pow(2, 1);

    printf("test pow 2^1 = %d \n", test);

    test = gf_pow(2, 0);

    printf("test pow 2^0 = %d \n", test);

    printf("Test Poly : \n\n");

    printf("Test RS_generator_poly : \n");

    struct poly *g = RS_generator_poly(4);

    printf("Generator poly : \n");

    gf_print_poly(g);

    printf("\n TEST ENCODE : \n\n");

    char *c = "TEST";

    char *r = "r";

    r = RS_encode_msg(c, 4);

    printf("r = %s \n", r);

    char *a = "@uGv2'&p";

    char *b = "b";

    b = RS_encode_msg(a, 9);

    printf("b = %s \n", b);

    printf("\n");
    printf("----------------- \n");
    printf("END OF TEST \n \n");
    success(alert);
}

void test_angle(){
    char *alert = "find_angle()";
    int angle = 0;

	SDL_Surface *input_image = NULL;
	init_sdl();

	input_image = load_image("input/angles/qrcode30deg.png");
	binarize(input_image);

	angle = find_angle(input_image, 0);
    assert_int(angle, -30, alert);

	input_image = load_image("input/angles/qrcode45deg.png");
    binarize(input_image);

	angle = find_angle(input_image, 0);
    assert_int(angle, -45, alert);

	input_image = load_image("input/angles/qrcode0deg.png");
	binarize(input_image);

	angle = find_angle(input_image, 0);
    assert_int(angle, 0, alert);

	input_image = load_image("input/angles/qrcode15deg.png");
	binarize(input_image);

	angle = find_angle(input_image, 0);
    assert_int(angle, -15, alert);

	input_image = load_image("input/angles/qrcodeDirty.png");
	binarize(input_image);

	angle = find_angle(input_image, 0);
    assert_int(angle, -15, alert);

    SDL_FreeSurface(input_image);
    success(alert);
}

void stress_test(){
    char *alert = "stress_test()";
    int angle = 0;

    for (size_t index = 0; index < TEST_NB; index++){
        SDL_Surface *input_image = NULL;
        init_sdl();

        input_image = load_image("input/angles/qrcode30deg.png");
        binarize(input_image);

        angle = find_angle(input_image, 0);
        assert_int(angle, -30, alert);

        input_image = load_image("input/angles/qrcode45deg.png");
        binarize(input_image);

        angle = find_angle(input_image, 0);
        assert_int(angle, -45, alert);

        input_image = load_image("input/angles/qrcode0deg.png");
        binarize(input_image);

        angle = find_angle(input_image, 0);
        assert_int(angle, 0, alert);

        input_image = load_image("input/angles/qrcode15deg.png");
        binarize(input_image);

        angle = find_angle(input_image, 0);
        assert_int(angle, -15, alert);

        SDL_FreeSurface(input_image);
        progress(index, alert);
    }

    success(alert);
}

void stress_test_scale_up(){
    char *alert = "stress_test_scale_up()";

    SDL_Surface *input_image = NULL;
    SDL_Surface *screen = NULL;
    SDL_Surface *new_surf = NULL;
    init_sdl();

    input_image = load_image("input/QR6.png");

    matrix *input_matrix = image_to_matrix(input_image);
    input_matrix = matrix_resize(input_matrix);
    doubleMatrix *double_input = simple_to_double(input_matrix);

    for(size_t index = 0; index < 6; index++){
        double_input = doublematrix_scale_up(double_input, 2);

        new_surf = double_to_surface(double_input);
        screen = display_image(new_surf);
        sleep(1);
    }

    SDL_FreeSurface(input_image);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(new_surf);
    matrix_free(input_matrix);
    doublematrix_free(double_input);
    SDL_Quit();
    success(alert);
}

void test_surface_to_bmp(){
    char *alert = "test_surface_to_bmp()";

    SDL_Surface *input_image = NULL;
    SDL_Surface *screen = NULL;
    SDL_Surface *new_surf = NULL;
    init_sdl();

    input_image = load_image("input/QR6.png");

    matrix *input_matrix = image_to_matrix(input_image);
    input_matrix = matrix_resize(input_matrix);
    doubleMatrix *double_input = simple_to_double(input_matrix);

    double_input = doublematrix_scale_up(double_input, 10);

    new_surf = double_to_surface(double_input);
    screen = display_image(new_surf);
    sleep(1);
    surface_to_image(new_surf, "output/QRCode.png");

    SDL_FreeSurface(input_image);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(new_surf);
    matrix_free(input_matrix);
    doublematrix_free(double_input);
    SDL_Quit();
    success(alert);
}

void test_doubleMatrix_init_blocks(){
    char *alert = "test_doubleMatrix_init_blocks()";

    doubleMatrix *matrix = doubleMatrix_init_blocks(25, 25);
    print_double_matrix(matrix);
    doublematrix_free(matrix);

    success(alert);
}

void test_dirty(){
    SDL_Surface *input_image = load_image("input/angles/qrcodeDirty.png");
    SDL_Surface *screen = NULL;

    binarize(input_image);
    int angle = find_angle(input_image, 0);
    if (angle != 0)
        input_image = rotozoomSurface(input_image, angle + 360, 1, 1);
    //input_image = remove_bg(input_image, 0);
    input_image = clean_image(input_image, 0);
    input_image = remove_bg(input_image, 0);

    screen = display_image(input_image);
    wait_for_keypressed();
    SDL_FreeSurface(input_image);
    SDL_FreeSurface(screen);
}
void test_suite(){
    test_xalloc();
    test_angle();
    //stress_test();
    stress_test_scale_up();
    test_surface_to_bmp();
    test_doubleMatrix_init_blocks();
    test_error_correction();
    test_dirty();
}

int main() {
    test_suite();

	return 0;
}
