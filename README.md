# What is QRunch ?

QRunch is C program designed to decode and encode QRCodes quickly. It's written in C to ensure maximum performance. You can use it on a server, or as a program by itself.

QRunch est un programme C qui permet de dechiffrer et d'encoder des QRCodes. QRunch est ecrit en C afin d'assurer une performance maximale. Vous pouvez l'utiliser sur un serveur, ou bien tel quel.

Les instructions en Francais sont a la fin du fichier.

# How to use

Make sure all the libraries required are installed and working on your machine.

These libraries are :

* sdl
* sdl_image
* sdl_gfx
* pkg-config

You can usually install them easily by using the following commands :

* pacman -S \<library's name\>
* apt-get install \<library's name\>

Clone the repository and access it :

`git clone https://gitlab.com/cohenarthur/qrunch && cd qrunch/`

Enter the following command :

* `make`

Three binary files are created : `nogui`, `qrunch` and `test`

Each of them serves a different purpose :

* `qrunch` uses a GUI
* `nogui` is a command line tool
* `test` simply runs the unit tests for the program. They can take multiple minutes

Run `./nogui -h` for more options !

# Comment l'utiliser

Tout d'abord, assurez vous que les bibliotheques necessaires sont installees sur votre machine

Ces bibliotheques sont :
* sdl
* sdl_image
* sdl_gfx
* pkg-config

Si elles ne sont pas installees, installez les en suivant la procedure suivante

* pacman -S \<library's name\>
* apt-get install \<library's name\>

Clonez la repo et allez dans le dossier :

`git clone https://gitlab.com/cohenarthur/qrunch && cd qrunch/`

Entrez la commande suivante :

* `make`

Trois binaires sont crees : `nogui`, `qrunch` and `test`

Chacun realise une fonction differente

* `qrunch` utilise une interface graphique
* `nogui` est un outil du shell
* `test` realise tous les tests unitaires du programme

Utilisez `./nogui -h` pour plus d'options !
