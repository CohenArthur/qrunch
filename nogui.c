#include <stdio.h>
#include <err.h>
#include <string.h>

#include "src/sdl_functions.h"
#include "src/image_treatment.h"
//#include "src/file_operations.h"
#include "src/matrix.h"

#include "src/struct_qr.h"
#include "src/decode.h"
#include "src/encode.h"

#include "src/color.h"

#include "src/cl_args.h"

#include "src/surface_to_image.h"

int main(int argv, char **argc)
{
	int verbose = 0;
	int encode = 0;
	char *argument = get_args(argv, argc, &verbose, &encode);

	if(encode) {
		encode_cli(argument);
	} else {
		decode_cli(argument, verbose);
	}

	free(argument);

	return 0;
}
