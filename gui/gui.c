#include "../src/gui.h"
#include "../src/cl_args.h"

#include "../src/xalloc.h"

#include <stdio.h>
#include <err.h>
#include <string.h>
#include <SDL/SDL.h>

#include "../src/sdl_functions.h"
#include "../src/sdl_functions.h"
#include "../src/image_treatment.h"
#include "../src/matrix.h"

#include "../src/struct_qr.h"
#include "../src/decode.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_IMPLEMENTATION
#define NK_XLIB_IMPLEMENTATION
#include "../src/nuklear.h"
#include "../src/nuklear_x11.h"

//#include "other_panels.c"

#define DTIME           20
#define WINDOW_WIDTH    800
#define WINDOW_HEIGHT   600
#define BUTTON_WIDTH    375

typedef struct XWindow XWindow;
struct XWindow {
    Display *dpy;
    Window root;
    Visual *vis;
    Colormap cmap;
    XWindowAttributes attr;
    XSetWindowAttributes swa;
    Window win;
    int screen;
    XFont *font;
    unsigned int width;
    unsigned int height;
    Atom wm_delete_window;
};

static void
die(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputs("\n", stderr);
    exit(EXIT_FAILURE);
}

static long
timestamp(void)
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL) < 0) return 0;
    return (long)((long)tv.tv_sec * 1000 + (long)tv.tv_usec/1000);
}

static void
sleep_for(long t)
{
    struct timespec req;
    const time_t sec = (int)(t/1000);
    const long ms = t - (sec * 1000);
    req.tv_sec = sec;
    req.tv_nsec = ms * 1000000L;
    while(-1 == nanosleep(&req, &req));
}

/* ===============================================================
 *
 *                          EXAMPLE
 *
 * ===============================================================*/
/* This are some code examples to provide a small overview of what can be
 * done with this library. To try out an example uncomment the defines */
/*#define INCLUDE_ALL */
/*#define INCLUDE_STYLE */
/*#define INCLUDE_CALCULATOR */


#ifdef INCLUDE_ALL
  #define INCLUDE_STYLE
  #define INCLUDE_CALCULATOR
  #define INCLUDE_OVERVIEW
  #define INCLUDE_NODE_EDITOR
#endif

#ifdef INCLUDE_STYLE
  #include "../style.c"
#endif
#ifdef INCLUDE_CALCULATOR
  #include "../calculator.c"
#endif

#ifdef INCLUDE_OVERVIEW
  #include "../overview.c"
#endif
#ifdef INCLUDE_NODE_EDITOR
  #include "../node_editor.c"
#endif

#define ORANGE		 255, 80, 40, 255
#define ORANGE_H	 255, 80, 20, 255
#define PURPLE		 180, 40, 255, 255
#define PURPLE_H	 180, 90, 255, 255

enum theme {THEME_BLACK, THEME_PURPLE, THEME_ORANGE};

static void
set_style(struct nk_context *ctx, enum theme theme)
{
    struct nk_color table[NK_COLOR_COUNT];
    if (theme == THEME_ORANGE) {
        table[NK_COLOR_TEXT] = nk_rgba(255, 255, 255, 255);
        table[NK_COLOR_WINDOW] = nk_rgba(40, 40, 40, 255);
        table[NK_COLOR_HEADER] = nk_rgba(20, 20, 20, 255);
        table[NK_COLOR_BORDER] = nk_rgba(10, 10, 10, 255);
        table[NK_COLOR_BUTTON] = nk_rgba(ORANGE);
        table[NK_COLOR_BUTTON_HOVER] = nk_rgba(ORANGE_H);
        table[NK_COLOR_BUTTON_ACTIVE] = nk_rgba(40, 40, 40, 255);
        table[NK_COLOR_TOGGLE] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_TOGGLE_HOVER] = nk_rgba(60, 60, 60, 255);
        table[NK_COLOR_TOGGLE_CURSOR] = nk_rgba(ORANGE);
        table[NK_COLOR_SELECT] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SELECT_ACTIVE] = nk_rgba(ORANGE);
        table[NK_COLOR_SLIDER] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SLIDER_CURSOR] = nk_rgba(ORANGE);
        table[NK_COLOR_SLIDER_CURSOR_HOVER] = nk_rgba(ORANGE_H);
        table[NK_COLOR_SLIDER_CURSOR_ACTIVE] = nk_rgba(ORANGE);
        table[NK_COLOR_PROPERTY] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_EDIT] = nk_rgba(51, 55, 67, 225);
        table[NK_COLOR_EDIT_CURSOR] = nk_rgba(190, 190, 190, 255);
        table[NK_COLOR_COMBO] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_CHART] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_CHART_COLOR] = nk_rgba(170, 40, 60, 255);
        table[NK_COLOR_CHART_COLOR_HIGHLIGHT] = nk_rgba(ORANGE);
        table[NK_COLOR_SCROLLBAR] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SCROLLBAR_CURSOR] = nk_rgba(ORANGE);
        table[NK_COLOR_SCROLLBAR_CURSOR_HOVER] = nk_rgba(ORANGE_H);
        table[NK_COLOR_SCROLLBAR_CURSOR_ACTIVE] = nk_rgba(ORANGE);
        table[NK_COLOR_TAB_HEADER] = nk_rgba(20, 20, 20, 255);
        nk_style_from_table(ctx, table);
    } else if (theme == THEME_PURPLE) {
        table[NK_COLOR_TEXT] = nk_rgba(255, 255, 255, 255);
        table[NK_COLOR_WINDOW] = nk_rgba(40, 40, 40, 215);
        table[NK_COLOR_HEADER] = nk_rgba(20, 20, 20, 255);
        table[NK_COLOR_BORDER] = nk_rgba(10, 10, 10, 255);
        table[NK_COLOR_BUTTON] = nk_rgba(PURPLE);
        table[NK_COLOR_BUTTON_HOVER] = nk_rgba(PURPLE_H);
        table[NK_COLOR_BUTTON_ACTIVE] = nk_rgba(40, 40, 40, 255);
        table[NK_COLOR_TOGGLE] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_TOGGLE_HOVER] = nk_rgba(60, 60, 60, 255);
        table[NK_COLOR_TOGGLE_CURSOR] = nk_rgba(PURPLE);
        table[NK_COLOR_SELECT] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SELECT_ACTIVE] = nk_rgba(PURPLE);
        table[NK_COLOR_SLIDER] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SLIDER_CURSOR] = nk_rgba(PURPLE);
        table[NK_COLOR_SLIDER_CURSOR_HOVER] = nk_rgba(PURPLE_H);
        table[NK_COLOR_SLIDER_CURSOR_ACTIVE] = nk_rgba(PURPLE);
        table[NK_COLOR_PROPERTY] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_EDIT] = nk_rgba(51, 55, 67, 225);
        table[NK_COLOR_EDIT_CURSOR] = nk_rgba(190, 190, 190, 255);
        table[NK_COLOR_COMBO] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_CHART] = nk_rgba(51, 55, 67, 255);
        table[NK_COLOR_CHART_COLOR] = nk_rgba(170, 40, 60, 255);
        table[NK_COLOR_CHART_COLOR_HIGHLIGHT] = nk_rgba(PURPLE);
        table[NK_COLOR_SCROLLBAR] = nk_rgba(50, 50, 50, 255);
        table[NK_COLOR_SCROLLBAR_CURSOR] = nk_rgba(PURPLE);
        table[NK_COLOR_SCROLLBAR_CURSOR_HOVER] = nk_rgba(PURPLE_H);
        table[NK_COLOR_SCROLLBAR_CURSOR_ACTIVE] = nk_rgba(PURPLE);
        table[NK_COLOR_TAB_HEADER] = nk_rgba(20, 20, 20, 255);
        nk_style_from_table(ctx, table);
    } else {
        nk_style_default(ctx);
    }
}


/* ===============================================================
 *
 *                          DEMO
 *
 * ===============================================================*/
void display_result(char *filename){
        SDL_Surface *image = load_image(filename);
        SDL_Surface *screen = NULL;

        init_sdl();

        screen = display_image(image);
        wait_for_keypressed();

        SDL_FreeSurface(image);
        SDL_FreeSurface(screen);

        SDL_Quit();
}


int
nuklear_main(void)
{
    long dt;
    long started;
    int running = 1;
    XWindow xw;
    struct nk_context *ctx;


	/* X11 */
    memset(&xw, 0, sizeof xw);
    xw.dpy = XOpenDisplay(NULL);
    if (!xw.dpy) die("Could not open a display; perhaps $DISPLAY is not set?");
    xw.root = DefaultRootWindow(xw.dpy);
    xw.screen = XDefaultScreen(xw.dpy);
    xw.vis = XDefaultVisual(xw.dpy, xw.screen);
    xw.cmap = XCreateColormap(xw.dpy,xw.root,xw.vis,AllocNone);

    xw.swa.colormap = xw.cmap;
    xw.swa.event_mask =
        ExposureMask | KeyPressMask | KeyReleaseMask |
        ButtonPress | ButtonReleaseMask| ButtonMotionMask |
        Button1MotionMask | Button3MotionMask | Button4MotionMask | Button5MotionMask|
        PointerMotionMask | KeymapStateMask;
    xw.win = XCreateWindow(xw.dpy, xw.root, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0,
        XDefaultDepth(xw.dpy, xw.screen), InputOutput,
        xw.vis, CWEventMask | CWColormap, &xw.swa);

    XStoreName(xw.dpy, xw.win, "QRunch QRCode Analyzer");
    XMapWindow(xw.dpy, xw.win);
    xw.wm_delete_window = XInternAtom(xw.dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(xw.dpy, xw.win, &xw.wm_delete_window, 1);
    XGetWindowAttributes(xw.dpy, xw.win, &xw.attr);
    xw.width = (unsigned int)xw.attr.width;
    xw.height = (unsigned int)xw.attr.height;

    /* GUI */
    xw.font = nk_xfont_create(xw.dpy, "fixed");
    ctx = nk_xlib_init(xw.font, xw.dpy, xw.screen, xw.win, xw.width, xw.height);

    set_style(ctx, THEME_PURPLE);
    int theme = 0;
    char *encode_buf = xcalloc(1, 256);
    char *decode_buf = xcalloc(1, 4096);
    char *res = xcalloc(1, 4096);
    char *encrypt_output = xcalloc(1, 4096);
    static int nb_files = 0;

    while (running) //Tant que la fenetre n'est pas fermee
    {
        /* Input */ XEvent evt;
        started = timestamp();
        nk_input_begin(ctx);
        while (XPending(xw.dpy))
		{
            XNextEvent(xw.dpy, &evt);
            if (evt.type == ClientMessage) goto cleanup;
            if (XFilterEvent(&evt, xw.win)) continue;
            nk_xlib_handle_event(xw.dpy, xw.screen, xw.win, &evt);
        }

/*NK FLAGS: nuklear-ui.github.io
 *
 *NK_WINDOW +=
 *
 * _BACKGROUND
 * _BORDER		: Adds a tiny border around the window
 * _CLOSEABLE
 * _CLOSED
 * _DYNAMIC		: Minimum possible size regarding the window content
 * _HIDDEN
 * _MAX_NAME
 * _MINIMIZABLE
 * _MINIMIZED
 * _MOVABLE		: You can move the window inside the X11 box
 * _NO_INPUT
 * _NO_SCROLLBAR
 * _NOT_INTERACTIVE
 * _PRIVATE
 * _ROM
 * _SCALABLE		: Adds a scale drag-point
 * _SCALE_LEFT		: Scale drag-point on the left instead of the right
 * _SCROLL_AUTO_HIDE
 * _TITLE
 * */

        /* GUI */
        if (nk_begin(ctx, "Encode", nk_rect(WINDOW_WIDTH/2, 0, WINDOW_WIDTH/2, WINDOW_HEIGHT),
        NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_BORDER|NK_WINDOW_CLOSABLE)){
            nk_layout_row_static(ctx, 100, BUTTON_WIDTH, 1);
            struct nk_input *in = &ctx->input;
            struct nk_rect bounds = nk_widget_bounds(ctx);
            if (nk_input_is_mouse_hovering_rect(in, bounds)){
                nk_tooltip(ctx, "Encrypt your data in a QR Code.");
            }
            if (nk_button_label(ctx, "ENCRYPT")){
                if (strlen(encode_buf) != 0){
                    if (strlen(encode_buf) > 17)
                        strcpy(encrypt_output, "The message is too long !");
                    else {
                        strcpy(encrypt_output, encode_gui(encode_buf, nb_files));
                        ++nb_files;
                    }
                }
            }

            nk_layout_row_static(ctx, 50, 0, 1);
            nk_layout_row_static(ctx, 50, BUTTON_WIDTH, 1);
            nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD|NK_EDIT_GOTO_END_ON_ACTIVATE,
            encode_buf, 256 * sizeof(encode_buf) - 1, nk_filter_default);
            nk_layout_row_static(ctx, 50, BUTTON_WIDTH/2, 2);
            if(nk_button_label(ctx, encrypt_output))

            nk_layout_row_static(ctx, 50, BUTTON_WIDTH, 1);
            if (strlen(encode_buf) != 0 && strlen(encrypt_output) != 0
                && strlen(encode_buf) <= 17){
                if(nk_button_label(ctx, "Display")){
                    display_result(encrypt_output);
                }
            }

            nk_layout_row_static(ctx, 200, 0, 1);
            nk_layout_row_static(ctx, 50, BUTTON_WIDTH, 1);
            ///Bouton QUIT
            in = &ctx->input;
            bounds = nk_widget_bounds(ctx);
            if (nk_button_label(ctx, "Quit"))
            break;
        }
        nk_end(ctx);

        if (nk_begin(ctx, "Decode", nk_rect(0, 0, WINDOW_WIDTH/2, WINDOW_HEIGHT),
        NK_WINDOW_TITLE|NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_BORDER|NK_WINDOW_CLOSABLE)){

            nk_layout_row_static(ctx, 100, BUTTON_WIDTH, 1);
            const struct nk_input *in = &ctx->input;
            struct nk_rect bounds = nk_widget_bounds(ctx);
            if (nk_input_is_mouse_hovering_rect(in, bounds)){
                nk_tooltip(ctx, "Decryption of a specified QR Code.");
            }
            if (nk_button_label(ctx, "DECRYPT")){
                if (strlen(decode_buf) != 0)
                    strcpy(res, decode_gui(decode_buf));
            }

            nk_layout_row_static(ctx, 50, 0, 1);
            nk_layout_row_static(ctx, 50, BUTTON_WIDTH, 1);
            nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD|NK_EDIT_GOTO_END_ON_ACTIVATE,
                decode_buf, 256 * sizeof(decode_buf) - 1, nk_filter_default);

            nk_layout_row_static(ctx, 50, BUTTON_WIDTH/2, 2);
            if(nk_button_label(ctx, res)){
            }
            if (strlen(res) != 0) {
                if(nk_button_label(ctx, "Display in Terminal")){
                    printf("%s\n", res);
                }
            }
            nk_layout_row_static(ctx, 200, 0, 1);
            nk_layout_row_static(ctx, 50, BUTTON_WIDTH, 1);
            in = &ctx->input;
            bounds = nk_widget_bounds(ctx);
            if(nk_button_label(ctx, "Theme")){
                if (theme%3 == 0) {
                    set_style(ctx, THEME_ORANGE);
                    theme++;
                } else if (theme%3 == 1){
                    set_style(ctx, THEME_BLACK);
                    theme++;
                } else {
                    set_style(ctx, THEME_PURPLE);
                    theme++;
                };
            }
        }

        nk_end(ctx);

        if (nk_window_is_hidden(ctx, "Decode") ||
        nk_window_is_hidden(ctx, "Encode")) break;

        /* -------------- EXAMPLES ---------------- */
        #ifdef INCLUDE_CALCULATOR
          calculator(ctx);
        #endif
        #ifdef INCLUDE_OVERVIEW
          overview(ctx);;

        #endif
        #ifdef INCLUDE_NODE_EDITOR
          node_editor(ctx);
        #endif
        /* ----------------------------------------- */

        /* Draw */
        XClearWindow(xw.dpy, xw.win);
        nk_xlib_render(xw.win, nk_rgb(40, 40, 40));
        XFlush(xw.dpy);

        /* Timing */
        dt = timestamp() - started;
        if (dt < DTIME)
            sleep_for(DTIME - dt);
	}//END_OF_WHILE

cleanup:
    free(res);
    free(encrypt_output);
    free(decode_buf);
    free(encode_buf);
    nk_xfont_del(xw.dpy, xw.font);
    nk_xlib_shutdown();
    XUnmapWindow(xw.dpy, xw.win);
    XFreeColormap(xw.dpy, xw.cmap);
    XDestroyWindow(xw.dpy, xw.win);
    XCloseDisplay(xw.dpy);
    return 0;
}
