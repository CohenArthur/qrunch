CC=gcc
CPPFLAGS= `pkg-config --cflags sdl`
CFLAGS= -Wall -Wextra -Werror -std=c99 -g

LDLIBS= `pkg-config --libs sdl` -lSDL_image -lSDL_gfx -lm -lpng -lX11

SRC_DIR=./src/

ALL_C=$(wildcard $(SRC_DIR)*.c)

BSRC=nogui.c ${ALL_C}
BOBJ=${BSRC:*.c=*.o}

TSRC=test.c ${ALL_C}
TOBJ=${TSRC:*.c=*.o}

QSRC=qrunch.c ./gui/gui.c ${ALL_C}
QOBJ=${QSRC:*.c=*.o}

all: qrunch nogui test

nogui: ${BOBJ}

qrunch: ${QOBJ}

test: ${TOBJ}

clena: clean

clean:
	${RM} mainBASH main nogui qrunch test *.o *.d
