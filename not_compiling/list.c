#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#include "list.h"

struct link_list *new_ll(int size)
{
    struct link_list *r = xmalloc(sizeof(struct link_list));

    r->data = size;

    struct link_list *index;
    struct link_list *stock = r->next;

    for(int i = 0; i < size; i++)
    {
        index = xmalloc(sizeof(struct link_list));
        index->data = 0;

        stock->next = index;
        stock = index;       
    }

    return r;    
}


